FROM openjdk:jre
COPY target/*.jar /helloworld.jar
CMD java -jar /helloworld.jar HelloworldApplication